import numpy as np

def lbg(X, k, eps=1e-6):
    """Perform vector quantization using the LBG algorithm."""
    # Initialize the codebook with the mean of the data points
    cb = np.mean(X, axis=0, keepdims=True)
    while cb.shape[0] < k:
        # Double the size of the codebook by splitting each centroid into two
        cb = np.vstack([cb*(1+eps), cb*(1-eps)])
        # Assign each data point to the closest centroid
        dist = np.sum((X[:, np.newaxis, :] - cb)**2, axis=2)
        labels = np.argmin(dist, axis=1)
        # Update the centroids to be the mean of the assigned data points
        for i in range(cb.shape[0]):
            cb[i] = np.mean(X[labels == i], axis=0)
    return cb, labels
